//Configures the AWS provider
provider "aws"{} //Select one from this link: https://registry.terraform.io/browse/providers
#1. It is advisable not to hard code this in config file because keys are supposed to be secret.
#2. Providers like AWS need to be installed cuz they're not included in the TF dl. It's like installing dependencies.
#3. Type "terraform init" in the powershell this folder this file is in to install aws provider so you can use commands from that provider.

//See "providers.tf" file for more info.
#7. Upon installing the AWS provider, you can use all features AWS has as a code.

#C3L3&4
variable "all_info" {
  description = "parameters of all resources"
  type = list(object({
    cidr_block = string
    name = string
  }))
}
variable "AZ_Singapore" {}

resource "aws_vpc" "sample_vpc" { //The syntax goes as follows: resource "<provider>_<resource type>" "<name>" 
  cidr_block = var.all_info[0].cidr_block //IP address range for the VPC resource. The lower the number beside the slash, the more IP addresses you can use.
  tags = {
    Name: var.all_info[0].name//Gives the resource a name in AWS.
  } 
}

//Create a subnet which is a resource that depends on another resource, the VPC.
resource "aws_subnet" "sample_SN" { //If you want to use a subnet from AWS named "sample_SN"
    vpc_id = aws_vpc.sample_vpc.id //The syntax goes as follows: vpc_id = <provider>_<resource type>.<name>.id
    cidr_block = var.all_info[1].cidr_block //IP range of the subnet from the VPC.
    availability_zone = var.AZ_Singapore //Defines the AZ name.
    tags = {
    Name: var.all_info[1].name
  }
}

/*Shows what info powershell will display upon a resource's creation.*/
output "all" {
  value = {
    id_vpc = aws_vpc.sample_vpc.id //Syntax: <name> = <provider>_<resource>.<name of resource>.<what attribute to show (e.g. id, owner_id)>
    owner_id_vpc = aws_vpc.sample_vpc.owner_id
    id_SN = aws_subnet.sample_SN.id
    owner_id_SN = aws_subnet.sample_SN.owner_id
  } 
}

/*#Ignores the local .terraform dir
.terraform/*

#Ignores the terraform state files
.tfstate
.tfstate.*

#Ignores the .tfvars files
.tfvars*/